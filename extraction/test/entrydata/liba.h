#ifndef __LIB_A_H__
#define __LIB_A_H__

typedef void (*fptr)(void);



void f8(void);
void f7(void);
void f6(void);
void f5(void);
void f4(void);
void f3(void);
fptr f2(void);
fptr f1(void);

#endif
